//
//  ServerSide.swift
//  Wallpaper day
//
//  Created by Илья Руденцов on 24.06.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//Test
//Universal
//Production

import Foundation
import UIKit

import Parse
import Bolts

class ServerSide: NSObject {
    let className = "Production"
    
    func parseCreateBase(){
        var message:PFObject = PFObject(className:"Test")
        message["image"] = "http://178.62.116.25/index.php/s/yUQ1FzxV83HxLy4/download"
        message["likes"] = 0
        message.saveInBackground()
    
    }
    
    func parseGetImageURL(){
        var query = PFQuery(className:className)
        query.orderByDescending("createdAt")
        query.getFirstObjectInBackgroundWithBlock { (data: PFObject?, error: NSError?) -> Void in
            if error == nil && data != nil {
                var imageURL = data?.objectForKey("image") as! String
                var count = data?.objectForKey("likes") as! Int
                var id:String = data!.objectId!
                
                globalArrayID.removeAllObjects()
                globalArrayID.addObject(id)

                globalImageUrl = imageURL
                globalLikes = count
                globalID = id
                NSNotificationCenter.defaultCenter().postNotificationName("detectLiked", object: id, userInfo: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("updateLikesLabel", object: count, userInfo: nil)
                println(imageURL)
                NSNotificationCenter.defaultCenter().postNotificationName("updateImage", object: imageURL, userInfo: nil)
                globalArrayImages = NSMutableArray()
                globalArrayImages.removeAllObjects()
                globalStop=false
                globalSkip=0
                globalLimit=18
                ServerSide().parseGetAllImagesURLs(globalSkip,limit: globalLimit)
                Purchase().requestFullVersion(false)
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0;
            } else {
                if(error?.code == 100){self.Error(3)}else{self.Error(1)}
            }
        }
    }
    
    func parseGetAllImagesURLs(skip:Int, limit:Int){
        globalStop = true
        if(globalSkip == 0){
        globalArrayImages.removeAllObjects()
        globalArrayID.removeAllObjects()
        }
        var query = PFQuery(className:className)
        query.orderByDescending("createdAt")
        query.skip = skip
        query.limit = limit
        query.findObjectsInBackgroundWithBlock{ (data: [AnyObject]?, error: NSError?) -> Void in
            if error == nil && data != nil {
                globalStop = false
                var array = NSMutableArray(array: data!)
                if(array.count < globalLimit){
                   globalStop = true
                }
                for var i=0;i<array.count;i+=1 {
                    var object:PFObject = array.objectAtIndex(i) as! PFObject
                    var imageUrl = object.objectForKey("image") as! String
                    var likes = object.objectForKey("likes") as! Int
                    var id = object.objectId!
                    println(imageUrl)
                    globalArrayLikes.addObject(likes)
                    globalArrayImages.addObject(imageUrl)
                    globalArrayID.addObject(id)
                }
                NSNotificationCenter.defaultCenter().postNotificationName("updateAllImages", object: nil, userInfo: nil)
            } else {
                globalStop = false
                println(error)
                if(error?.code == 100){self.Error(3)}else{self.Error(1)}
            }
        }
    }
    
    
    func parsePostLike(id:String) {
        var query = PFQuery(className:className)
        query.getObjectInBackgroundWithId(id, block: { (data: PFObject?, error: NSError?) -> Void in
            if error != nil {
                println(error)
                 if(error?.code == 100){self.Error(3)}else{self.Error(1)}
            } else if let data = data {
                var imageURL = data.objectForKey("image") as! String
                var count = data.objectForKey("likes") as! Int
                var id = data.objectId!
                
                globalArrayLikes.replaceObjectAtIndex(globalArrayID.indexOfObject(id), withObject: count + 1)
                NSNotificationCenter.defaultCenter().postNotificationName("updateLikesLabel", object: count + 1, userInfo: nil)
                data["likes"] = count + 1
                data.saveInBackgroundWithBlock({ (Bool, error: NSError?) -> Void in
                    if error != nil {
                        println(error)
                        if(error?.code == 100){self.Error(3)}else{self.Error(1)}
                    } else{
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: id)
                        NSNotificationCenter.defaultCenter().postNotificationName("Liked", object: nil, userInfo: nil)
                        NSNotificationCenter.defaultCenter().postNotificationName("detectLiked", object: id, userInfo: nil)
                        println("Liked")
                    }
                    
                })
            }
        })
    }
//
    func Error(i:Int){
        var text = String()
        if(i==1){     text = "Server crashed. :("}
        else if(i==2){text = "Hosting crashed. :("}
        else if(i==3){text = "No Internet. :("}
        NSNotificationCenter.defaultCenter().postNotificationName("Error", object: text, userInfo: nil)
    }
    
    func deviceModel(screen:CGRect)->CGFloat{
        var model = String()
        var height = screen.height*2
        var c = CGFloat()
        
        if(height == 960){
        model = "iphone4"
        c = 5
        }else if(height == 1136){
        model = "iphone5"
        c = 4
        }else if(height == 1334){
        model = "iphone6"
        c = 3.5
        }else if(height == 1472){
        model = "iphone6plus"
        c = 3
        }else{model = "unknown: \(screen.height)"}
        
        println(model)
        return c
    }
}