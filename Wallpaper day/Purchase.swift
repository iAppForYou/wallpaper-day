//
//  Purchase.swift
//  Wallpaper day
//
//  Created by Илья Руденцов on 07.07.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import RMStore
import MBProgressHUD
import SpinKit

class Purchase:NSObject{
    var productID = "Wallpaper_day.Full_version"
    
    func requestFullVersion(buy:Bool) {
         if(buy == true){self.ProgressHUD()}
        RMStore.defaultStore().requestProducts(NSSet(array: [productID]) as Set<NSObject>, success: { (object1:[AnyObject]!, object2:[AnyObject]!) -> Void in
            
            var arr:NSArray = object1 as NSArray
            var product:SKProduct = arr.objectAtIndex(0) as! SKProduct
            
            let formatter = NSNumberFormatter()
            formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
            formatter.locale = product.priceLocale
            println(formatter.stringFromNumber(product.price))
            
            globalProductPrice = formatter.stringFromNumber(product.price)!
            
            if(buy == true){self.buyFullVersion()}
            
            }) { (error:NSError!) -> Void in
            self.hud.hidden=true
        }
    }
    
    func buyFullVersion() {
        RMStore.defaultStore().addPayment(productID, success: { (transaction:SKPaymentTransaction!) -> Void in
            println("Purchased.")
            self.hud.hidden=true
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "keyBuyFullVersion")
            }) { (transaction:SKPaymentTransaction!, error:NSError!) -> Void in
            self.hud.hidden=true
            println(error)
        }
    }
    
    func restoreFullVersion(){
        self.ProgressHUD()
        RMStore.defaultStore().restoreTransactionsOnSuccess({ (sender:[AnyObject]!) -> Void in
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "keyBuyFullVersion")
            self.restoreCompleate()
            self.hud.hidden=true
            }, failure: { (error:NSError!) -> Void in
            self.restoreFail()
            self.hud.hidden=true
        })
    }
    
    func restoreCompleate(){
        var alert = UIAlertController(title: NSLocalizedString("Your purchase has been restored :)", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "ok", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
        })
        alert.addAction(cancel)
        
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func restoreFail(){
        var alert = UIAlertController(title: NSLocalizedString("You did not make purchases :(", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "ok", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
        })
        alert.addAction(cancel)
        
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    var hud = MBProgressHUD()
    func ProgressHUD(){
        var spinner = RTSpinKitView(style: RTSpinKitViewStyle.StyleThreeBounce, color: UIColor.whiteColor())
        
        hud = MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow, animated: false)
        hud.square = true
        hud.removeFromSuperViewOnHide = true
        hud.mode = MBProgressHUDMode.CustomView
        hud.customView = spinner
        //hud.labelText = NSLocalizedString("Loading", "Loading")
        
        //UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(hud, animated: true, completion: nil)
        spinner.startAnimating()
    }
    
    
}
