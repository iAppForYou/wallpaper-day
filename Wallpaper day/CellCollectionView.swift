//
//  CellCollectionView.swift
//  Wallpaper day
//
//  Created by Илья Руденцов on 05.07.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class CellCollectionView: UICollectionViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    
}