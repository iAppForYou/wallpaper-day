//
//  Rate.swift
//  Wallpaper day
//
//  Created by Илья Руденцов on 07.07.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import Foundation
import UIKit

class Rate: NSObject {
    var appId = "1009652833"
    var freeCount = 6
    
    func rateAlert(indexPath:NSIndexPath){
        var alert = UIAlertController(title: nil, message: NSLocalizedString("To gain access to all content required Full Version", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancel = UIAlertAction(title: NSLocalizedString("Not now", comment: ""), style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
        })
        var freecount = NSUserDefaults.standardUserDefaults().integerForKey("keyFreeCount")
        let getFree = UIAlertAction(title: NSLocalizedString("Get free", comment: "")+" \(freecount)", style: .Default, handler: { (action) -> Void in
            NSUserDefaults.standardUserDefaults().setInteger(NSUserDefaults.standardUserDefaults().integerForKey("keyFreeCount") - 1, forKey: "keyFreeCount")
            NSNotificationCenter.defaultCenter().postNotificationName("getFree", object: indexPath, userInfo: nil)
        })
        let rate = UIAlertAction(title: NSLocalizedString("Rate", comment: ""), style: .Default, handler: { (action) -> Void in
            self.rateGoRate()
        })
        let buy = UIAlertAction(title: NSLocalizedString("Full Version", comment: "") + " \(globalProductPrice)", style: .Default, handler: { (action) -> Void in
            self.rateAlertFullVersion()
        })
        
        
        alert.addAction(cancel)
        if(NSUserDefaults.standardUserDefaults().boolForKey("keyRated") == false){
        alert.title = NSLocalizedString("Please rate the app in the App Store and get \(freeCount) FREE", comment: "")
        alert.addAction(rate)
        }else if(NSUserDefaults.standardUserDefaults().integerForKey("keyFreeCount") > 0){
            
            alert.addAction(getFree)
        }
        
        alert.addAction(buy)
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func rateAlertFullVersion(){
        var alert = UIAlertController(title: NSLocalizedString("To gain access to all content required Full Version", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
        })
        var textbuy = NSLocalizedString("Buy", comment: "")
        let buy = UIAlertAction(title: "\(textbuy) \(globalProductPrice)", style: .Default, handler: { (action) -> Void in
            Purchase().requestFullVersion(true)
        })
        let restore = UIAlertAction(title: NSLocalizedString("Restore full version", comment: ""), style: .Default, handler: { (action) -> Void in
            Purchase().restoreFullVersion()
        })
        
        alert.addAction(cancel)
        alert.addAction(buy)
        alert.addAction(restore)
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }

    
    func rateGoRate(){
        var iTunesLink = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(appId)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"
        UIApplication.sharedApplication().openURL(NSURL(string: iTunesLink)!)
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "keyRated")
        NSUserDefaults.standardUserDefaults().setInteger(freeCount, forKey: "keyFreeCount")
    }
    
    
}