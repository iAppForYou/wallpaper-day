//
//  ViewController.swift
//  Wallpaper day
//
//  Created by Илья Руденцов on 20.06.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
import SpinKit
import QuartzCore
import Spring
import SDWebImage
import Parse

var globalSkip = Int()
var globalLimit = Int()
var globalImageUrl = String()
var globalArrayImages = NSMutableArray()
var globalLikes = Int()
var globalArrayLikes = NSMutableArray()
var globalID = String()
var globalArrayID = NSMutableArray()
var globalStop = Bool()
var globalProductPrice = String()

class TodayVC: UIViewController, UITableViewDelegate, UICollectionViewDelegate {
   
    @IBOutlet weak var imageStripe: UIImageView!
    @IBOutlet weak var buttonAllWallpaper: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonRefresh: UIButton!
    @IBOutlet weak var labelHeader: SpringLabel!
    @IBOutlet weak var buttonLike: UIButton!
    ///@IBOutlet weak var headerImage: UIImageView!
    ///@IBOutlet weak var footerImage: UIImageView!
    @IBOutlet weak var blurViewTop: UIView!
    @IBOutlet weak var blurViewBottom: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var wallpaperImageView: UIImageView!
    
    var spinnerTop = RTSpinKitView()
    var spinnerBottom = RTSpinKitView()
    var boolHideHeader = Bool()
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "updateImage", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "updateLikesLabel", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "Liked", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "detectLiked", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "updateAllImages", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "Error", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Notification:", name: "getFree", object: nil)
        
        spinnerTop = RTSpinKitView(style: RTSpinKitViewStyle.StyleThreeBounce, color: UIColor.whiteColor())
        spinnerTop.center = CGPointMake(self.view.frame.width/2, blurViewTop.frame.height/2)
        self.blurViewTop.addSubview(spinnerTop)
        
        ServerSide().parseGetImageURL()
        
        boolHideHeader=true
        buttonSave.enabled = false
        buttonLike.enabled = false
        buttonAllWallpaper.enabled = false
        
        blurViewBottom.frame = CGRectMake(blurViewBottom.frame.origin.x, blurViewBottom.frame.origin.y, blurViewBottom.frame.size.width,self.view.frame.height - self.view.frame.height/4)
        
        spinnerBottom = RTSpinKitView(style: RTSpinKitViewStyle.StyleThreeBounce, color: UIColor.whiteColor())
        spinnerBottom.center = CGPointMake(self.view.frame.width/2, blurViewBottom.frame.size.height - 14)
        self.blurViewBottom.addSubview(spinnerBottom)
        spinnerBottom.stopAnimating()
        createBlur()
        
        //buy()
        //self.createBase()
        //NSUserDefaults.standardUserDefaults().setBool(false, forKey: "keyRated")
    }
    
//deleteMe
    func buy(){
        Purchase().requestFullVersion(true)
    }
    
    func createBase(){
        for var i=0;i<1000;i+=1{
        ServerSide().parseCreateBase()
        }
    }
//
    func Notification(notification:NSNotification){
        if(notification.name=="updateImage"){self.updateImage(notification.object as! String)}
        if(notification.name=="updateLikesLabel"){self.updateLikes(notification.object as! Int)}
        if(notification.name=="Liked"){self.showLabelHeader("Liked!", showRefresh: false)}
        if(notification.name=="detectLiked"){self.detectLiked(notification.object as! String)}
        if(notification.name=="getFree"){self.selectImage(notification.object as! NSIndexPath)}
        if(notification.name=="updateAllImages"){
            spinnerBottom.stopAnimating()
            self.collectionView.reloadData()
        }
        
        if(notification.name=="Error"){
            self.showLabelHeader(notification.object as! String, showRefresh: true)
        }
    }
    
    func updateImage(url: String){
        globalImageUrl = url
        spinnerTop.startAnimating()
        //UpdateHeaderAndFooterNoBlur()
        self.wallpaperImageView.sd_setImageWithURL(NSURL(string: url), completed: { (image:UIImage!, error:NSError!, cache:SDImageCacheType, url:NSURL!) -> Void in
            if(error==nil){
            self.buttonSave.enabled = true
            self.buttonAllWallpaper.enabled = true
            self.spinnerTop.stopAnimating()
            //self.UpdateHeaderAndFooter(image)
            }else{
                ServerSide().Error(2)
            }
        })
    }
    func createBlur(){
        var style = UIBlurEffectStyle.Dark
        var darkBlur = UIBlurEffect(style: style)
        var darkBlurView = UIVisualEffectView(effect: darkBlur)
        darkBlurView.frame = blurViewTop.frame
        self.blurViewTop.insertSubview(darkBlurView, belowSubview: buttonSave)
        
        var darkBlurBottom = UIBlurEffect(style: style)
        var darkBlurViewBottom = UIVisualEffectView(effect: darkBlur)
        darkBlurViewBottom.frame = CGRectMake(0, 0, blurViewBottom.frame.size.width,self.view.frame.height - self.view.frame.height/4)
        self.blurViewBottom.insertSubview(darkBlurViewBottom, belowSubview: imageStripe)
    }

    
    func updateLikes(count:Int){
         buttonLike.setTitle("\(count)", forState: UIControlState.Normal)
    }
    
    func detectLiked(id:String){
        if(NSUserDefaults.standardUserDefaults().boolForKey(id) == true){
            self.buttonLike.enabled = false
        }else{
            self.buttonLike.enabled = true
        }
    }
    
    func showLabelHeader(text:String, showRefresh:Bool){
        spinnerTop.stopAnimating()
        if(self.buttonRefresh.hidden == true){
        labelHeader.text=text
        labelHeader.hidden=false
        labelHeader.force=0.1
        labelHeader.duration=0.2
        labelHeader.animation = "fadeIn"
        labelHeader.animate()
        labelHeader.delay=3.0
        labelHeader.animation = "fadeOut"
        labelHeader.animateNext { () -> () in
            self.buttonSave.enabled = true
            if(showRefresh==true){
            self.buttonRefresh.hidden = false
            self.buttonSave.enabled = false
            self.buttonLike.enabled = false}
        }
        }
    }
    
//BUttons
    @IBAction func buttonDownloadUp(sender: UIButton) {
        sender.enabled = false
        var image = UIImageView(frame: self.wallpaperImageView.frame)
        image.image = wallpaperImageView.image
        self.view.insertSubview(image, aboveSubview: wallpaperImageView)
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            image.layer.cornerRadius = 10
            image.frame = sender.frame
            }) { (Bool) -> Void in
                image.removeFromSuperview()
        }
        UIImageWriteToSavedPhotosAlbum(wallpaperImageView.image, self, "image:didFinishSavingWithError:contextInfo:", nil)
    }
    @IBAction func buttonLikeUp(sender: UIButton) {
        spinnerTop.startAnimating()
        sender.enabled=false
        ServerSide().parsePostLike(globalID)
    }
    @IBAction func buttonRefreshUp(sender: UIButton) {
        sender.hidden = true
        spinnerTop.startAnimating()
        ServerSide().parseGetImageURL()
    }
    @IBAction func buttonShowAll(sender: UIButton) {
            tableView.setContentOffset(CGPointMake(0, tableView.tableHeaderView!.frame.height + blurViewBottom.frame.size.height + UIApplication.sharedApplication().statusBarFrame.height ), animated: true)
    }
    @IBAction func buttonCollectionViewTopUp(sender: UIButton) {
        collectionView.setContentOffset(CGPointZero, animated: true)
    }
    
//Other
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if error == nil {
            showLabelHeader(NSLocalizedString("Saved in Photos.", comment: ""), showRefresh: false)
            
        } else {
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.sharedApplication().openURL(url)
            }
            //showLabelHeader(NSLocalizedString("Not saved :(", comment: ""), showRefresh: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//table
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(boolHideHeader == true){
            tableView.setContentOffset(CGPointMake(0, tableView.tableHeaderView!.frame.height   + UIApplication.sharedApplication().statusBarFrame.height ), animated: true)
            boolHideHeader=false}else{
            tableView.setContentOffset(CGPointMake(0, 0), animated: true)
            boolHideHeader=true}
    }
    
//Collect
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return globalArrayImages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectCell", forIndexPath: indexPath) as! CellCollectionView
        cell.backgroundColor = UIColor.whiteColor()
        cell.activityIndicator.startAnimating()
        cell.imageView.sd_setImageWithURL(NSURL(string: globalArrayImages.objectAtIndex(indexPath.row) as! String), placeholderImage: nil) { (image:UIImage!, error:NSError!, cache:SDImageCacheType, url:NSURL!) -> Void in
            if(error==nil){
                cell.activityIndicator.stopAnimating()
            }else{
                ServerSide().Error(2)
            }
        }
        
        cell.layer.borderColor = UIColor(white: 1.0, alpha: 1.0).CGColor
        if(globalImageUrl == globalArrayImages.objectAtIndex(indexPath.row) as! String){
            cell.layer.borderWidth = 4
        }else{
            cell.layer.borderWidth = 0
        }
        if(globalStop == false){
        if(globalArrayImages.count == indexPath.row + 1){
            globalSkip = globalArrayImages.count
            spinnerBottom.startAnimating()
            
            let thread = NSThread(target:self, selector:"loadMoreImages:", object:nil)
            thread.start()
        }
            println("\(globalArrayImages.count)==\(indexPath.row+1)")
        }
        return cell
    }
    func loadMoreImages(object:AnyObject){
        ServerSide().parseGetAllImagesURLs(globalSkip, limit: globalLimit)
    }
    func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        var cell:UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
            cell.layer.borderWidth = 4
    }
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        if(globalImageUrl != globalArrayImages.objectAtIndex(indexPath.row) as! String){
        var cell:UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        cell.layer.borderWidth = 0
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var cell:CellCollectionView = collectionView.cellForItemAtIndexPath(indexPath) as! CellCollectionView
        
        if(NSUserDefaults.standardUserDefaults().boolForKey("keyBuyFullVersion") == false && indexPath.row > 0){
        Rate().rateAlert(indexPath)
        }else if(cell.imageView.image != nil){
            cell.layer.borderWidth = 0
            selectImage(indexPath)
        }

    }
    
    func selectImage(indexPath:NSIndexPath){
        
        tableView.setContentOffset(CGPointMake(0, 0), animated: true)
        
        
        self.wallpaperImageView.sd_setImageWithURL(NSURL(string: globalArrayImages.objectAtIndex(indexPath.row) as! String), completed: { (image:UIImage!, error:NSError!, cache:SDImageCacheType, url:NSURL!) -> Void in
            if(error == nil){
                //let thread = NSThread(target:self, selector:"UpdateHeaderAndFooter:", object:image)
                //thread.start()
            }else{
                ServerSide().Error(2)
            }
        })
        
        updateLikes(globalArrayLikes.objectAtIndex(indexPath.row) as! Int)
        
        globalImageUrl = globalArrayImages.objectAtIndex(indexPath.row) as! String
        globalID = globalArrayID.objectAtIndex(indexPath.row) as! String
        
        detectLiked(globalArrayID.objectAtIndex(indexPath.row) as! String)
        collectionView.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width / 3.04, height: self.view.frame.size.width / 3.04)
    }

}

