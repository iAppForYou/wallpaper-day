
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 1

// RMStore
#define COCOAPODS_POD_AVAILABLE_RMStore
#define COCOAPODS_VERSION_MAJOR_RMStore 0
#define COCOAPODS_VERSION_MINOR_RMStore 7
#define COCOAPODS_VERSION_PATCH_RMStore 1

// RMStore/Core
#define COCOAPODS_POD_AVAILABLE_RMStore_Core
#define COCOAPODS_VERSION_MAJOR_RMStore_Core 0
#define COCOAPODS_VERSION_MINOR_RMStore_Core 7
#define COCOAPODS_VERSION_PATCH_RMStore_Core 1

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

// SpinKit
#define COCOAPODS_POD_AVAILABLE_SpinKit
#define COCOAPODS_VERSION_MAJOR_SpinKit 1
#define COCOAPODS_VERSION_MINOR_SpinKit 2
#define COCOAPODS_VERSION_PATCH_SpinKit 0

// Spring
#define COCOAPODS_POD_AVAILABLE_Spring
#define COCOAPODS_VERSION_MAJOR_Spring 1
#define COCOAPODS_VERSION_MINOR_Spring 0
#define COCOAPODS_VERSION_PATCH_Spring 3

